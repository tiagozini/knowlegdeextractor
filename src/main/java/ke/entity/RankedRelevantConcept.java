package ke.entity;

public class RankedRelevantConcept {
	
	private Category category;
	
	private CategoryEvaluation categoryEvaluation;
	
	private long qtdConcepts;
	
	private long qtdSubCategories;
	
	public RankedRelevantConcept(CategoryEvaluation catEva) {
		setCategoryEvaluation(catEva);
		setCategory(catEva.getCategory());
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public CategoryEvaluation getCategoryEvaluation() {
		return categoryEvaluation;
	}

	public void setCategoryEvaluation(CategoryEvaluation categoryEvaluation) {
		this.categoryEvaluation = categoryEvaluation;
	}

	public long getQtdConcepts() {
		return qtdConcepts;
	}

	public void setQtdConcepts(long qtdConcepts) {
		this.qtdConcepts = qtdConcepts;
	}

	public long getQtdSubCategories() {
		return qtdSubCategories;
	}

	public void setQtdSubCategories(long qtdSubCategories) {
		this.qtdSubCategories = qtdSubCategories;
	}

}

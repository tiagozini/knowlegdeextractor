package ke.entity;

public class EndPoint {

	private String url;
	
	public EndPoint(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}

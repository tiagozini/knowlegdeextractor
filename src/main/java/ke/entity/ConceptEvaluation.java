package ke.entity;

public class ConceptEvaluation {
	
	private Concept concept;
	
	private Double votesSum;
	
	private Double score;
	
	private Long frequency;

	public ConceptEvaluation(Concept concept) {
		setConcept(concept);
		setScore(0.0D);
		setFrequency(0L);
		setVotesSum(0.0D);	
	}

	public Double getVotesSum() {
		return votesSum;
	}

	public void setVotesSum(Double votesSum) {
		this.votesSum = votesSum;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Long getFrequency() {
		return frequency;
	}

	public void setFrequency(Long frequency) {
		this.frequency = frequency;
	}

	public Concept getConcept() {
		return concept;
	}

	public void setConcept(Concept concept) {
		this.concept = concept;
	}

	@Override
	public String toString() {
		return this.getConcept().toString();
	}
	
	@Override
	public int hashCode() {
		return getConcept().hashCode();
	}	
	
	@Override
	public boolean equals(Object obj) {
		return ((ConceptEvaluation) obj).getConcept().equals(this.getConcept());		
	}

}

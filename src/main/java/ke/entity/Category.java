package ke.entity;

public class Category {

	private String uri;

	public Category(String conURI) {
		setUri(conURI);
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
	@Override
	public String toString() {
		return this.uri.toString();
	}
	
	@Override
	public int hashCode() {
		return this.uri.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return  this.uri.equals(((Category) obj).getUri());
	}
}

package ke.entity;

import java.io.Serializable;
import java.util.Set;

//@Entity
public class ResourceLink implements Serializable {

	private static final long serialVersionUID = 1L;

	private String uri;	

	private Integer id;
	
	private Set<ResourceLink> wikiLinks;

	//@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}

	//@ManyToMany(fetch=FetchType.LAZY)
	public Set<ResourceLink> getWikiLinks() {
		return wikiLinks;
	}

	public void setWikiLinks(Set<ResourceLink> wikiLinks) {
		this.wikiLinks = wikiLinks;
	}	
}

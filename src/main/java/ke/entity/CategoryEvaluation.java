package ke.entity;

public class CategoryEvaluation {
	
	private Category category;
	
	private Double votesSum;
	
	private Double score;
	
	private Long frequency;
	
	private Long qtdConcepts;
	
	private Long qtdSubCategories;	

	public CategoryEvaluation(Category cat) {
		setCategory(cat);
		setScore(0.0D);
		setFrequency(0L);
		setVotesSum(0.0D);
		setQtdConcepts(0L);
		setQtdSubCategories(0L);
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Double getVotesSum() {
		return votesSum;
	}

	public void setVotesSum(Double votesSum) {
		this.votesSum = votesSum;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Long getFrequency() {
		return frequency;
	}

	public void setFrequency(Long frequency) {
		this.frequency = frequency;
	}

	public Long getQtdConcepts() {
		return qtdConcepts;
	}

	public void setQtdConcepts(Long qtdConcepts) {
		this.qtdConcepts = qtdConcepts;
	}

	public Long getQtdSubCategories() {
		return qtdSubCategories;
	}

	public void setQtdSubCategories(Long qtdSubCategories) {
		this.qtdSubCategories = qtdSubCategories;
	}		

	@Override
	public String toString() {
		return this.getCategory().toString();
	}
	
	@Override
	public int hashCode() {
		return getCategory().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return  this.getCategory().equals(((CategoryEvaluation) obj).getCategory());
	}
}

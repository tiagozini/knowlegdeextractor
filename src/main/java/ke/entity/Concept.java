package ke.entity;

public class Concept {
	
	private String descricao;
	
	private String wikipageUrl;
	
	private String dbpediaPage;

	public Concept(String descricao) {
		this.setDescricao(descricao);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getWikipageUrl() {
		return wikipageUrl;
	}

	public void setWikipageUrl(String wikipageUrl) {
		this.wikipageUrl = wikipageUrl;
	}

	public String getDbpediaPage() {
		return dbpediaPage;
	}

	public void setDbpediaPage(String dbpediaPage) {
		this.dbpediaPage = dbpediaPage;
	}
	
	@Override
	public String toString() {
		return this.getDbpediaPage().toString();
	}	
	
	@Override
	public boolean equals(Object obj) {
		return ((Concept) obj).getDbpediaPage().equals(this.getDbpediaPage());		
	}
	
	@Override
	public int hashCode() {
		return this.getDbpediaPage().hashCode();
	}

}

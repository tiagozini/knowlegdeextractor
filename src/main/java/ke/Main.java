package ke;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openrdf.model.Statement;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;

class SparqlClient {
	
	public static final String PREFIX_OPEN_DATA = "" + "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
			+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" 
			+ "PREFIX qb: <http://purl.org/linked-data/cube#>\n"
			+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
			+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
			+ "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n"
			+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n";
	
	public static final String QUERY__GET_CONCEPT_WITH_SIMILAR_LABEL = "select distinct ?obj ?name where {\n"
			+ "?obj rdfs:label ?name.\n" 
			+ "FILTER regex(?name, \"%Conceito%\").\n"
			+ " } LIMIT 1";
	
	public static final String DBPEDIA_ENDPEDIA = "http://dbpedia.org/sparql";
	
	public static void connection(String endpoint, String sp)
			throws RepositoryException, QueryEvaluationException, MalformedQueryException {
		Repository Remoterepo = new SPARQLRepository(endpoint);
		RepositoryConnection con = null;		
		String sparqlQuery = PREFIX_OPEN_DATA + sp;
		
		System.out.println("Endpoint:" + endpoint);
		System.out.println("Query: " + sparqlQuery);
		try {
			Remoterepo.initialize();
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}
		try {
			con = Remoterepo.getConnection();
		} catch (RepositoryException e2) {
			e2.printStackTrace();
		}
		try {
			GraphQuery graphQuery = con.prepareGraphQuery(QueryLanguage.SPARQL, sparqlQuery);
			GraphQueryResult gresult;
			try {
				gresult = graphQuery.evaluate();
				try {
					while (gresult.hasNext()) {
						Statement statement = gresult.next();
						System.out.println(statement.toString());
					}
				} finally {
					try {
						gresult.close();
					} catch (Exception e) {
						 e.printStackTrace();
					}
					con.close();
				}
			} catch (QueryEvaluationException e1) {
				e1.printStackTrace();
			}
		} catch (RepositoryException e3) {
			 e3.printStackTrace();
		}
		Remoterepo.shutDown();
	}	
}

/**
 * Example program to list links from a URL.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Validate.isTrue(args.length == 1, "usage: supply url to fetch");
        //SparqlClient.connection(SparqlClient.DBPEDIA_ENDPEDIA, SparqlClient.QUERY__GET_CONCEPT_WITH_SIMILAR_LABEL.replace("%Conceito%", "Poverty"));
        return;
    }
    
    private static void findWikiLinks(String url) throws IOException {
        print("Fetching %s...", url);

        Document doc = Jsoup.connect(url).get();
        Elements links = doc.select("a[href]");
        List<String> wikiLInks = new ArrayList<String>();
        List<String> categoryWikiLInks = new ArrayList<String>();        
        for (Element link : links) {
        	if (link.attr("abs:href").contains("en.wikipedia.org/wiki")) {
        		wikiLInks.add(link.attr("abs:href"));
        		if (link.attr("abs:href").contains("Category:")) {
        			categoryWikiLInks.add(link.attr("abs:href"));
        		}
        	}
        }
        print("\nWikiLinks: (%d)", links.size());        
        for (String link : wikiLInks) {
       		print(" * a: %s", link);
        }
        
        print("\nCatLinks: (%d)", links.size());        
        for (String cat : categoryWikiLInks) {
       		print(" * a: %s", cat);
        }        
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;
    }
}


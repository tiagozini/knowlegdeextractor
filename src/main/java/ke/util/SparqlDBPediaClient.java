package ke.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;

import ke.entity.Concept;

public class SparqlDBPediaClient {
	
	public static final String PREFIX_OPEN_DATA = "" + "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
			+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" 
			+ "PREFIX qb: <http://purl.org/linked-data/cube#>\n"
			+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
			+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
			+ "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n"		
			+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n"
			+ "PREFIX dbpedia-owl: <http://dbpedia.org/ontology>\n"			
			+ "PREFIX prov: <http://www.w3.org/ns/prov#>\n";
	
	public static final String QUERY__GET_CONCEPTS_WITH_SIMILAR_LABEL = "select distinct ?obj ?name ?wikipage where { \n "+
		  "?obj rdfs:label ?name.\n " +
		  "?obj prov:wasDerivedFrom ?wikipage\n" +		  
		  "FILTER regex(?name, \"%%TEXT%%\").\n " +		  
		"} LIMIT 100\n";
	
	public static final String QUERY__GET_CONCEPT_WITH_RESOURCE = "select distinct ?name ?wikipage where { \n "+
			  "<%%RESOURCE%%> rdfs:label ?name.\n " +
			  "<%%RESOURCE%%> prov:wasDerivedFrom ?wikipage\n" +		  
			"} LIMIT 1\n";
	
	public static final String QUERY__GET_CONCEPTS_FROM_INLINKS = "SELECT DISTINCT ?obj ?name ?wikipage WHERE { \n "+
			  "?obj dbpedia-owl:wikiPageWikiLink <%%RESOURCE%%>.\n " +
			  "?obj rdfs:label ?name.\n " +
			  "?obj prov:wasDerivedFrom ?wikipage.\n " +			  
			"}\n";
	
	public static final String QUERY__GET_CONCEPTS_FROM_OUTLINKS = "SELECT DISTINCT ?obj ?name ?wikipage WHERE { \n "+
			  "<%%RESOURCE%%> dbpedia-owl:wikiPageWikiLink ?obj.\n " +		
			  "?obj rdfs:label ?name.\n " +
			  "?obj prov:wasDerivedFrom ?wikipage.\n " +
			"}\n";
	
	public static final String QUERY__GET_CONCEPTS_OF_CATEGORY = "SELECT DISTINCT ?concept, ?label, ?wiki WHERE { " +
		  "?concept dct:subject <%%CATEGORY_URI%%> ." +
		  " ?concept rdfs:label ?label . " +
		  " ?concept prov:wasDerivedFrom ?wiki . " +
		  " FILTER langMatches(lang(?label), \"%%LANG%%\") " +
		  "} ";	
	
	public static final String QUERY__GET_CATEGORIES_FROM_CONCEPT = "SELECT ?sub WHERE { <%%CONCEPT_URI%%> dct:subject ?sub  }";
	
	public static final String QUERY__COUNT_CONCEPTS_OF_CATEGORY = "select distinct count(?Concept) AS ?qtd  where {?Concept dct:subject <%%CATEGORY_URI%%> } ";
	
	public static final String QUERY__COUNT_SUBCATEGORY_OF_CATEGORY = "select distinct count(?SubCat) AS ?qtd where {?SubCat skos:broader <%%CATEGORY_URI%%>}";
	
	public static final String DBPEDIA_ENDPOINT_DEFAULT = "http://dbpedia.org/sparql";

	public static final String DBPEDIA_DEFAULT_URL = "http://dbpedia.org/resource";	
	
	public static final String DBPEDIA_SEARCH_URL = "http://lookup.dbpedia.org/api/search.asmx/KeywordSearch";
	
	public static final String DBPEDIA_SEARCH_QUERY_PARAMETER = "QueryString";
	
	public static final String DBPEDIA_SEARCH_CATEGORY_PARAMETER = "QueryClass";
	
	public static final String SETUP_CALL = "&timeout=30000";

	private static final int TIMEOUT = 0;

	private static final boolean DEBUG = false;	
	
	public RepositoryConnection con;
	
	public Repository remoteRepo;

	private String endpointUrl;

	public SparqlDBPediaClient(String endPoint) {
		this.setEndpointUrl(endPoint);
	}	
	
	public SparqlDBPediaClient() {
		this.setEndpointUrl(DBPEDIA_ENDPOINT_DEFAULT);
	}
	
	public Concept runGetConceptWithSimilarLabel(String text) {
		Concept concept = null;
		try {
			connect();
			String queryText = QUERY__GET_CONCEPTS_WITH_SIMILAR_LABEL.replace("%%TEXT%%", text);
			Map<String, Concept> conceptMap = new HashMap<String, Concept>();
			List<HashMap<String, String>> results = query(queryText, new String[]{"name", "obj", "wikipage"});
			for(HashMap<String, String> map : results) {
				Concept c = new Concept(map.get("name"));
				c.setWikipageUrl(map.get("wikipage"));
				c.setDbpediaPage(map.get("obj"));
				conceptMap.put(c.getDescricao(), c);
			}
			String bestMatch = new StringMatcher(text).findBestPhraseMatch(Arrays.asList((String[]) conceptMap.keySet().toArray(new String[conceptMap.keySet().size()])));			
			concept = conceptMap.get(bestMatch);			
		} catch(Exception e) {			
			System.out.println(e.getMessage());
		} finally {
			//close();						
		}
		return concept;
	}	
	
	public Set<Concept> findConceptsOfCategory(String categoryURI) {
		
		Set<Concept> concepts = new HashSet<Concept>();
		try {
			connect();
			String queryText = QUERY__GET_CONCEPTS_OF_CATEGORY.replace("%%CATEGORY_URI%%", categoryURI).replace("%%LANG%%", "en");
			for(HashMap<String, String> map : query(queryText, new String[]{"label", "wiki", "concept"})) {
				Concept concept = new Concept(map.get("label"));
				concept.setWikipageUrl(map.get("wiki"));
				concept.setDbpediaPage(map.get("concept"));
				concepts.add(concept);
			}		
		} catch(Exception e) {			
			System.out.println(e.getStackTrace());
			return concepts;
		} finally {
			//close();						
		}
		return concepts;		
	}
	
	public Concept runGetConceptWithResource(String resourceName) {
		Concept concept = null;
		try {
			connect();
			String queryText = QUERY__GET_CONCEPT_WITH_RESOURCE.replace("%%RESOURCE%%", resourceName);
			for(HashMap<String, String> map : query(queryText, new String[]{"name", "wikipage"})) {
				concept = new Concept(map.get("name"));
				concept.setWikipageUrl(map.get("wikipage"));
				concept.setDbpediaPage(resourceName);
			}		
		} catch(Exception e) {			
			System.out.println(e.getStackTrace());
		} finally {
			//close();						
		}
		return concept;
	}	
	
	public void connect() {
		if (con == null || !con.isOpen()) {
			remoteRepo = new SPARQLRepository(endpointUrl);
			con = null;		
			//System.out.println("Endpoint:" + endpointUrl);
	
			try {
				remoteRepo.initialize();
			} catch (RepositoryException e1) {
				e1.printStackTrace();
			}
			try {
				con = remoteRepo.getConnection();
			} catch (RepositoryException e2) {
				e2.printStackTrace();
			}		
		}
	}
	
	public void close() {
		con.close();
		remoteRepo.shutDown();
		con = null;
	}
	
	public List<HashMap<String, String>> query(String sp, String[] fields) throws RepositoryException, QueryEvaluationException, MalformedQueryException {
		List<HashMap<String, String>> lista = new ArrayList<HashMap<String, String>>();		
		try {
			String sparqlQuery = PREFIX_OPEN_DATA + sp;
			if (DEBUG)
				System.out.println(sparqlQuery);
			TupleQuery tq = con.prepareTupleQuery(QueryLanguage.SPARQL, sparqlQuery);
			tq.setMaxExecutionTime(TIMEOUT);
			TupleQueryResult tqr = tq.evaluate();
			try {
	              while (tqr.hasNext()) {  // iterate over the result solutions
	                HashMap<String, String> map = new HashMap<String, String>();
	      			BindingSet bs = tqr.next();
	      			for (String fieldName : fields) {
		    			Value v = bs.getValue(fieldName);
		    			map.put(fieldName, v.stringValue());
	      			}
	      			lista.add(map);	    			
	            }
	  			return lista;	              
			} finally {
			      tqr.close(); // always close result to release associated resources				
			}
		} catch (RepositoryException e3) {
			 e3.printStackTrace();
		} catch (Exception e) {
			 e.printStackTrace();			
		}
		finally {
		}
		return null;
	}

	public String getEndpointUrl() {
		return endpointUrl;
	}

	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}

	public Set<Concept> runGetConceptsFromInLinks(Concept concept) {
		Set<Concept> concepts = new HashSet<Concept>();
		try {
			//connect();
			String queryText = QUERY__GET_CONCEPTS_FROM_INLINKS.replace("%%RESOURCE%%", concept.getDbpediaPage());
			for(HashMap<String, String> map : query(queryText, new String[]{"name", "wikipage", "obj"})) {
				concept = new Concept(map.get("name"));
				concept.setWikipageUrl(map.get("wikipage"));
				concept.setDbpediaPage(map.get("obj"));
				concepts.add(concept);				
			}
		} catch(Exception e) {			
			System.out.println(e.getStackTrace());
		} finally {
			//close();						
		}
		return concepts;
	}
	
	public Long runCountSubCategoriesOfCategory(String categoryURI) {
		try {
			connect();
			String queryText = QUERY__COUNT_SUBCATEGORY_OF_CATEGORY.replace("%%CATEGORY_URI%%", categoryURI);
			for(HashMap<String, String> map : query(queryText, new String[]{"qtd"})) {
				return Long.valueOf(map.get("qtd"));				
			}
		} catch(Exception e) {			
			System.out.println(e.getStackTrace());
			return 0L;			
		} finally {
			//close();						
		}
		return 0L;
	}	
	
	public Long runCountConceptsOfCategory(String categoryURI) {
		try {
			connect();
			String queryText = QUERY__COUNT_CONCEPTS_OF_CATEGORY.replace("%%CATEGORY_URI%%", categoryURI);
			for(HashMap<String, String> map : query(queryText, new String[]{"qtd"})) {
				return Long.valueOf(map.get("qtd"));				
			}
		} catch(Exception e) {			
			System.out.println(e.getStackTrace());
			return 0L;			
		} finally {
			//close();						
		}
		return 0L;
	}	
	
	public Set<String> runGetCategoryNamesFromConcept(Concept concept) {
		Set<String> categoryNames = new HashSet<String>();
		try {
			connect();
			String queryText = QUERY__GET_CATEGORIES_FROM_CONCEPT.replace("%%CONCEPT_URI%%", concept.getDbpediaPage());
			for(HashMap<String, String> map : query(queryText, new String[]{"sub"})) {
				categoryNames.add(map.get("sub"));				
			}
		} catch(Exception e) {			
			System.out.println(e.getStackTrace());
		} finally {
			//close();						
		}
		return categoryNames;
	}	
	
	public Set<Concept> runGetConceptsFromOutLinks(Concept concept) {
		Set<Concept> concepts = new HashSet<Concept>();
		try {
			connect();
			String queryText = QUERY__GET_CONCEPTS_FROM_OUTLINKS.replace("%%RESOURCE%%", concept.getDbpediaPage());
			for(HashMap<String, String> map : query(queryText, new String[]{"name", "wikipage", "obj"})) {
				concept = new Concept(map.get("name"));
				concept.setWikipageUrl(map.get("wikipage"));
				concept.setDbpediaPage(map.get("obj"));
				concepts.add(concept);				
			}		
		} catch(Exception e) {			
			System.out.println(e.getStackTrace());
		} finally {
			//close();						
		}
		return concepts;
	}		
}

package ke.util;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class ConnectorSQL {
	
	private Connection conn;
	
	private static final String DB_NAME = "wikiLinksDb";
	private static final Integer DB_PORT = 3306;
	private static final String DB_HOST = "localhost";
	private static final String DB_DRIVER = "mysql";
	private static final String DB_USER = "root";	
	private static final String DB_PASS = "";		
	
	
	public ConnectorSQL() {		
		
	}	
	
	public Connection getConnection() {
		if (conn == null) {
			try {
				Class.forName(String.format("com.%s.jdbc.Driver", DB_DRIVER));
			} catch (ClassNotFoundException e) {
				System.out.println("Where is your JDBC Driver?");
				e.printStackTrace();
				return null;
			}
		
			System.out.println("JDBC Driver Registered!");
		
			try {
				conn = (Connection) DriverManager
				.getConnection(String.format("jdbc:%s://%s:%d/%s", DB_DRIVER, DB_HOST, DB_PORT, DB_NAME), DB_USER, DB_PASS);
		
			} catch (SQLException e) {
				System.out.println("Connection Failed! Check output console");
				e.printStackTrace();
				return null;
			}			
		}
		return conn;
	}
	
	public void close() throws SQLException {
		getConnection().close();
		System.out.println("Connection close successfuly!");
		
	}	
	
}

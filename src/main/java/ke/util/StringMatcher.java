package ke.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringMatcher {
	
	private String target;
	private String[] targetWords;
	
	public StringMatcher(String target) {
		this.setTarget(target);
		this.setTargetWords(target.toLowerCase().split("[^a-zA-Z0-9]+"));		
	}

	public String findMatchPhrase(String phrase) {
		int lastMatchPosition = 0;
		String matches = "";
		String[] words = phrase.toLowerCase().split("[^a-zA-Z0-9]+");		
		Boolean found = false;
		String str1 = "-";
		String str2 = "";
		String str3 = "%03d";
		String str4 = "999";		
		for(int targetPosition = 0; targetPosition < getTargetWords().length; targetPosition++) {
			found = false;
			for(int wordPosition = lastMatchPosition; wordPosition < words.length; wordPosition++) {
				if (getTargetWords()[targetPosition].equals(words[wordPosition])) {
					matches += (matches.length() > 0 ? str1 : str2) + String.format(str3, wordPosition);
					found = true;
					break;
				}
			}
			if (!found) {
				matches += str4;
			}
		}
		matches += (matches.length() > 0 ? "-" : "") + String.format("%03d", words.length);
		return matches;		
	}
	
	public String findBestPhraseMatch(List<String> phrases) {
		Map<String, String> phraseMap = new HashMap<String, String>();
		for(String phrase : phrases) {
			phraseMap.put(findMatchPhrase(phrase), phrase);
		}
		ArrayList<String> listaOrdenada = new ArrayList<String>(phraseMap.keySet());
		listaOrdenada.sort(new Comparator<String>(){
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		//List<String> listaOrdenada = Arrays.asList(lista);
		Collections.sort(listaOrdenada);
		
		return (listaOrdenada.size() > 0 ? phraseMap.get(listaOrdenada.get(0)) : null);
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String[] getTargetWords() {
		return targetWords;
	}

	public void setTargetWords(String[] targetWords) {
		this.targetWords = targetWords;
	}
	
}

package ke.algorithm;
import static java.util.logging.Level.ALL;
import static java.util.logging.Level.CONFIG;
import static java.util.logging.Level.FINE;
import static java.util.logging.Level.FINER;
import static java.util.logging.Level.FINEST;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.OFF;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import ke.entity.Category;
import ke.entity.CategoryEvaluation;
import ke.entity.Concept;
import ke.entity.ConceptEvaluation;

public class Teste {


    private static final Logger LOG = Logger.getLogger(Teste.class.getName());

    public static void m1() {
    	Level[] levels = {
                OFF, SEVERE, WARNING, INFO,
                CONFIG, FINE, FINER, FINEST, ALL
            };
            for (Level level : levels) {
                LOG.setLevel(level);
                LOG.log(level, "Hello Logger");
            }    	
    }
    
    public static void m2() {
    	Map<Category, CategoryEvaluation> categoriesMap = new HashMap<Category, CategoryEvaluation>();
    	String[] categoryNames = {"abc", "abcd", "abc", "abc", "abcd", "eaf"};
		for(String catName : categoryNames) {
			Category cat = new Category(catName);			
			CategoryEvaluation catEva = categoriesMap.get(cat);
			if (catEva == null) {
				catEva = new CategoryEvaluation(cat);									
			} else {
				System.out.println("Exce��o");				
			}
			categoriesMap.put(cat, catEva);
		}
    }
    
    public static void m3() {
    	Map<Concept, ConceptEvaluation> categoriesMap = new HashMap<Concept, ConceptEvaluation>();
    	String[] categoryNames = {"abc", "abcd", "abc", "abc", "abcd", "eaf"};
		for(String conceptName : categoryNames) {
			Concept con = new Concept("");
			con.setDbpediaPage(conceptName);
			ConceptEvaluation conEva = categoriesMap.get(con);
			if (conEva == null) {
				conEva = new ConceptEvaluation(con);									
			} else {
				System.out.println("Exce��o");				
			}
			categoriesMap.put(con, conEva);
		}
    }    
    
    public static void main(String[] args) {
        m3();
    }

}

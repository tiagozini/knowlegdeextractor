package ke.algorithm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.log4j.Level;

import javassist.bytecode.stackmap.TypeData.ClassName;
import ke.entity.Category;
import ke.entity.CategoryEvaluation;
import ke.entity.Concept;
import ke.entity.ConceptEvaluation;
import ke.entity.EndPoint;
import ke.ttl.Importator;
import ke.util.SparqlDBPediaClient;

public class AlgorithmOne {

	private static final String PATHFILE_PAGE_LINKS_EN_TTL = "C:\\Users\\Tiago\\Downloads\\Documents\\Mestrado\\Dados\\page_links_en.ttl";
	private static final String PREFIX_PFILE_LKS_IN_OUT = "C:\\Users\\Tiago\\Downloads\\Documents\\Mestrado\\Dados\\concepts_in_out_%%CONCEPT_URI%%.txt";	
	private static final String STR_LIST_OF = "List_of";
	private static final int LIMIT_OF_CATEGORIES_ANALISED = 250;
	private static final String CHARSET_UTF8 = "UTF-8";
	private static final String FILE__CONCEPT_RESOURCES_FOUND =  "C:\\Users\\Tiago\\Downloads\\Documents\\Mestrado\\Dados\\concept_resources_found.txt";
	private static final String FILE__LIST_CONCEPT_RESOURCES_FOUND =  "C:\\Users\\Tiago\\Downloads\\Documents\\Mestrado\\Dados\\list_concept_resources_found.txt";
	private static final Logger LOGGER = Logger.getLogger( ClassName.class.getName() );
			
	private Set<Concept> concept;	
	private EndPoint endpoint;	
		
	private void setup(String endPointUrl) {
		endpoint = new EndPoint(endPointUrl);		
	}
	
	private Concept getConceptFromStr(String query) {
		SparqlDBPediaClient sc = new SparqlDBPediaClient();
		
		return sc.runGetConceptWithSimilarLabel(query);
	}	

	private List<ConceptEvaluation> process(String endPoinUrl, String query) {
		setup(endPoinUrl);
		return search(query);		
	}
	
	private List<ConceptEvaluation> search(String query) {
		long m = 200;
		long min = 6;
		long max = 30;
		
		Concept concept = (Concept) getConceptFromStr(query);		
		Set<Concept> concepts = searchConceptsConnected(concept);
		Map<Category, CategoryEvaluation> scoredCategoriesMap = findScoredCategories(concepts, LIMIT_OF_CATEGORIES_ANALISED);
		SparqlDBPediaClient sc = new SparqlDBPediaClient();		
		sc.connect();
		// n = 200-250 m = 120-200
		
		System.out.println("Doing 'Create a list of relevant categories'");
		
		// create a list of relevante categories
		Set<Category> relevantCategories = new HashSet<Category>();
		int qtd = 0, itensQtd=0;	
		while(true) {
			qtd = 0;
			itensQtd = 0;
			for (Entry<Category, CategoryEvaluation> entry: scoredCategoriesMap.entrySet()) {
				CategoryEvaluation catEva = entry.getValue();
				
				if (catEva.getQtdConcepts() == 0)
					catEva.setQtdConcepts(sc.runCountConceptsOfCategory(catEva.getCategory().getUri()));
				itensQtd++;
				// Cut point to get better performance
				if (catEva.getQtdConcepts() >= m)
					continue;
				qtd++;
				
			}
			
			// Segue at� ter pelo menos 10 ou chegar no final da fila. 
			if(qtd >= 10 || itensQtd == scoredCategoriesMap.size()) {
				break;
			} else {
				m += 10;
			}
		} 
		for (Entry<Category, CategoryEvaluation> entry: scoredCategoriesMap.entrySet()) {
			CategoryEvaluation catEva = entry.getValue();
			
			if (catEva.getQtdConcepts() == 0)
				catEva.setQtdConcepts(sc.runCountConceptsOfCategory(catEva.getCategory().getUri()));
			
			// Cut point to get better performance
			if (catEva.getQtdConcepts() >= m)
				continue;
			
			catEva.setQtdSubCategories(sc.runCountSubCategoriesOfCategory(catEva.getCategory().getUri()));	
			
			if (min < catEva.getQtdSubCategories() && catEva.getQtdSubCategories() < max) {
				relevantCategories.add(catEva.getCategory());
			}
		}
		System.out.println("Doing 'build the relevant concepts list'");
		
		// bind extra concepts and rank them
		// lines 19 to 27 of original Algorithm 1 
		System.out.println("Doing 'bind extra concepts':");		
		Map<Concept, ConceptEvaluation> scoredConceptsMap = new HashMap<Concept, ConceptEvaluation>();
		
		for(Concept con : concepts) {
			ConceptEvaluation conEva = new ConceptEvaluation(con);
			conEva.setFrequency(1L);
			scoredConceptsMap.put(con, conEva);
		}		
		CategoryEvaluation catEva = null;
		for(Category relevantCat : relevantCategories) {
			Set<Concept> conceptsCat = sc.findConceptsOfCategory(relevantCat.getUri());
		    catEva = scoredCategoriesMap.get(relevantCat);
		    
			for(Concept con : conceptsCat) {
				ConceptEvaluation conEva = scoredConceptsMap.get(con);
				if (con.getDbpediaPage().equals("http://dbpedia.org/resource/List_of_countries_by_percentage_of_population_living_in_poverty")) {				
					System.out.println("Teste");
				}
				if (conEva == null) {
					conEva = new ConceptEvaluation(con);				
				} else {
					System.out.println("Exce��o conceito");
				}
				conEva.setFrequency(conEva.getFrequency() + 1L);
				conEva.setVotesSum(conEva.getVotesSum() + catEva.getScore());
				scoredConceptsMap.put(con, conEva);				
			}
		}
		
		// Calculate score of concepts - line 25 original Algorithm 1
		List<ConceptEvaluation> conEvaList = new ArrayList<ConceptEvaluation>(scoredConceptsMap.values());
		ConceptEvaluation conEva = null;
		
		for(Entry<Concept, ConceptEvaluation> ceEntry : scoredConceptsMap.entrySet()) {
			conEva = ceEntry.getValue();
			conEva.setScore(conEva.getVotesSum() * conEva.getFrequency());
			scoredConceptsMap.put(ceEntry.getKey(), conEva);
		}
		
		// Calculate score of concepts - line 26 original Algorithm 1		
		conEvaList.sort(new Comparator<ConceptEvaluation>() {
			@Override
			public int compare(ConceptEvaluation o1, ConceptEvaluation o2) {
				return o2.getScore().compareTo(o1.getScore());
			}			
		});
		List<ConceptEvaluation> listOfConcept = new ArrayList<ConceptEvaluation>();
		List<ConceptEvaluation> listOfConceptOfLists = new ArrayList<ConceptEvaluation>();		
		for(ConceptEvaluation c : conEvaList) {
			if (c.getConcept().getDbpediaPage().contains(STR_LIST_OF))
				listOfConceptOfLists.add(c);
			listOfConcept.add(c);			
		}

		String str1 = "%s(%.2f)\n";
		PrintWriter writer = null;
		System.out.println("---Todos os conceitos---");
		try {
			writer = new PrintWriter(FILE__CONCEPT_RESOURCES_FOUND,	CHARSET_UTF8);
			
			for (ConceptEvaluation ce : listOfConcept) {
				System.out.println(String.format(str1, ce.getConcept().getDbpediaPage(), ce.getScore()));
				writer.println(String.format(str1, ce.getConcept().getDbpediaPage(), ce.getScore()));
			}
			writer.close();			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				writer.close();			
		}

		System.out.println("---Todos os conceitos com list of---");		
		writer = null;
		try {
			writer = new PrintWriter(FILE__LIST_CONCEPT_RESOURCES_FOUND, CHARSET_UTF8);
			
			for (ConceptEvaluation ce : listOfConceptOfLists) {
				System.out.println(String.format(str1, ce.getConcept().getDbpediaPage(), ce.getVotesSum()));
				writer.println(String.format(str1, ce.getConcept().getDbpediaPage(), ce.getVotesSum()));
			}
			writer.close();			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				writer.close();			
		}		
		sc.close();
		return conEvaList;
	}
	
	private Map<Category, CategoryEvaluation> findScoredCategories(Set<Concept> concepts, int topLimit) {
		SparqlDBPediaClient sc = new SparqlDBPediaClient();
		sc.connect();
		Map<Category, CategoryEvaluation> categoriesMap = new HashMap<Category, CategoryEvaluation>();
		for(Concept con : concepts) {			
			Set<String> categoryNames = sc.runGetCategoryNamesFromConcept(con);
			double qtdCategories = categoryNames.size();
			
			// reject concepts that have no categories
			if (qtdCategories == 0)
				continue;
			double vote = 1.0/qtdCategories;
			for(String catName : categoryNames) {
				Category cat = new Category(catName);			
				CategoryEvaluation catEva = categoriesMap.get(cat);
				
				if (catEva ==  null) {
					catEva = new CategoryEvaluation(cat);					
				} else {
					System.out.println("Exce��o");
				}
				catEva.setCategory(cat);
				catEva.setVotesSum(catEva.getVotesSum() + vote);
				catEva.setFrequency(catEva.getFrequency() + 1L);
				categoriesMap.put(cat, catEva);				
			}			
		}		
		System.out.println("Doing 'calculate score for each category'");
		
		// Calculate score for each category
		for(Entry<Category, CategoryEvaluation> entry : categoriesMap.entrySet()) {
			CategoryEvaluation catEva = entry.getValue();
			
			catEva.setScore(catEva.getFrequency() * catEva.getVotesSum());
		}
		
		for(Entry<Category, CategoryEvaluation> entry : categoriesMap.entrySet()) {
			System.out.printf("Cat:%s - Score: %.2f, Votes: %.2f, Freq: %d\n", entry.getKey().getUri(), entry.getValue().getScore(),  entry.getValue().getVotesSum(), entry.getValue().getFrequency());
		}

		// return if the limit is out of need to be ordered
		if (topLimit > categoriesMap.size()) {
			sc.close();
			return categoriesMap;
		}
		
		System.out.println("Doing 'order category evaluation by score'");		
		// Order category evaluation by score
		ArrayList<CategoryEvaluation> list = new ArrayList<CategoryEvaluation>(categoriesMap.values());
		list.sort(new Comparator<CategoryEvaluation>() {
			@Override
			public int compare(CategoryEvaluation o1, CategoryEvaluation o2) {
				return o2.getScore().compareTo(o1.getScore());
			}			
		});	

		Map<Category, CategoryEvaluation> topSelectedCatsEva = new HashMap<Category, CategoryEvaluation>();
		CategoryEvaluation catEva = null;
		
		// Take categories until the topLimit quantity
		for (int i = 0; i < topLimit; i++) {
			catEva = list.get(i);
			topSelectedCatsEva.put(catEva.getCategory(), catEva);
		}
		sc.close();
		return topSelectedCatsEva;		
	}
	
	private Set<Concept> findLinksFromTtl(Concept concept) {
		String fpConcepts = PREFIX_PFILE_LKS_IN_OUT.replace("%%CONCEPT_URI%%", concept.getDescricao().toLowerCase());
		File f = new File(fpConcepts);
		Set<Concept> concepts = new HashSet<Concept>();
		String line = null;
		
		if (f.exists()) {
			Concept c = null;			
			try (BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()))) {
				while ((line = br.readLine()) != null) {
					if (!line.isEmpty()) {
						c = new Concept("");
						c.setDbpediaPage(line);
						concepts.add(c);
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			concepts = new Importator(PATHFILE_PAGE_LINKS_EN_TTL).findWikiLinkResources(concept.getDbpediaPage());
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(fpConcepts, "UTF-8");
				for(Concept c : concepts) {
					writer.println(c.getDbpediaPage());
				}				
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				e.printStackTrace();		
			} finally {
				if (writer != null) {
					writer.close();	
				}
			}
		}
		return concepts;
	}
	
	private Set<Concept> searchConceptsConnected(Concept concept) {	
		Set<Concept> concepts = findLinksFromTtl(concept);
		
		concepts.add(concept);
		return concepts;		
	}
	
	public EndPoint getEndpoint() {
		return endpoint;
	}

	public Set<Concept> getConcept() {
		return concept;
	}

	public void setConcept(Set<Concept> concept) {
		this.concept = concept;
	}
	
	public static void main(String[] args) {
		AlgorithmOne algOne = new AlgorithmOne();
		algOne.process(SparqlDBPediaClient.DBPEDIA_ENDPOINT_DEFAULT, "Poverty");		
	}
	
}

package ke.ttl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mysql.jdbc.Connection;

import ke.entity.Concept;
import ke.util.ConnectorSQL;

public class Importator {

	private static final long BLOCK_SIZE_TO_INFORM = 1000000L;
	private static final long MAX_ELEMENTS_IMPORTED = 10000000000L;
	private static final String SQL_PART__TWO_PARAMETERS = " (?, ?) ";
	private static final String REGEX_NOT_NUMBER = "[^0-9]+";
	private static final int SQL_BLOCK_SIZE = 5000;
	private static final String LINK_SEPARATOR = "|";
	private static final String PATH_SEPARATOR = "/";	
	private static final String COMMA_SEPARATOR = ",";
	private static final String DOUBLE_DOT_SEPARATOR = ":";	
	private static final String REGEX_RESOURCE_LINK = "\\s*<(http://dbpedia.org[^>]+)>\\s+\\S+\\s+<(http://dbpedia.org[^>]+)>.*";
	private static final String FILE_PATH_LINKS_FOUND = "C:\\Users\\Tiago\\Downloads\\Documents\\Mestrado\\Dados\\linksFound.txt";
	private static final String QUERY__TRUNCATE_TABLE_WIKILINK = "TRUNCATE TABLE wikilink";
	private static final String QUERY__TRUNCATE_TABLE_PREFIX = "TRUNCATE TABLE prefix";
	private static final String QUERY__TRUNCATE_TABLE_RESOURCELINK = "TRUNCATE TABLE resourcelink";
	private String name;

	public Importator(String name) {
		this.setName(name);
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

	@SuppressWarnings("finally")
	public Long verifyNumberOfLines() {
		Long i = 0L;
		try (BufferedReader br = new BufferedReader(new FileReader(name))) {
			while ((br.readLine()) != null) {
				i++;
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			return i;
		}
	}
	
	public void clearDatabase() throws SQLException {
		ConnectorSQL connector = new ConnectorSQL();
		Connection conn = connector.getConnection();
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(QUERY__TRUNCATE_TABLE_RESOURCELINK);
			ps.execute();		
			ps = conn.prepareStatement(QUERY__TRUNCATE_TABLE_PREFIX);
			ps.execute();					
			ps = conn.prepareStatement(QUERY__TRUNCATE_TABLE_WIKILINK);
			ps.execute();			
		} catch (SQLException e) {
			System.out.println("Erro ocorreu ao tentar limpar o banco de dados.");
			e.printStackTrace();
		} finally {
			connector.close();
		}
	}

	public Set<Concept> findWikiLinkResources(String resourceLink) {
		int i = 0;
		Set<String> inLinks = new HashSet<String>();
		Set<String> outLinks = new HashSet<String>();
		Set<Concept> list = new HashSet<Concept>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(name))) {
			String line;
			String toLink = "";
			String fromLink = "";		
			String str1 = "<" + resourceLink + ">";			
			Pattern r = Pattern.compile(REGEX_RESOURCE_LINK);
			Matcher m = null;
			int qtdFound = 0;
			Concept c = null;
			while ((line = br.readLine()) != null) {
				i++;
				if (i % 10000000L == 0) {
					System.out.println(i);
				}			
				if (i == 1 || !line.contains(str1)) {
					continue;
				}
				m = r.matcher(line);				
				if (!m.find()) {
					System.out.println("N�o encontrado o padr�o na linha: " + line);
					continue;
				} else {
					qtdFound++;
					fromLink = m.group(1);
					toLink = m.group(2);		
					if (toLink.equals(resourceLink)) {
						c = new Concept("");
						c.setDbpediaPage(fromLink);
						list.add(c);
					} else if(fromLink.equals(resourceLink)) {
						c = new Concept("");
						c.setDbpediaPage(toLink);
						list.add(c);
					} else {
						continue;
					}
				}
			}
			br.close();			
			System.out.println("Total de inlinks:" + inLinks.size());
			System.out.println("Total de outlinks:" + outLinks.size());			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
		return list;
	}	
	
	public Long importForDB() {
		Long i = 0L;
		Date dateStart = Calendar.getInstance().getTime();
		Map<String, Integer> linkIdMap = new HashMap<String, Integer>();
		Map<String, Integer> prefixesMap = new HashMap<String, Integer>();
		Integer linkCounter = 0;
		Integer prefixCounter = 0;
		PrintWriter writer = null;

		try (BufferedReader br = new BufferedReader(new FileReader(name))) {
			String line;
			String prefix = null;
			String posfix = null;
			String linkCompact = null;
			Integer idLink = null;
			Integer idFromLink = null;
			Integer idToLink = null;

			writer = new PrintWriter(FILE_PATH_LINKS_FOUND,	"UTF-8");
			StringBuilder sb = new StringBuilder();
			final String str1 = PATH_SEPARATOR;			
			while ((line = br.readLine()) != null) {
				i++;
				if (i == 1) {
					continue;
				}
				if (i % BLOCK_SIZE_TO_INFORM == 0) {
					System.out.println(i);
				}				
				Pattern r = Pattern.compile(REGEX_RESOURCE_LINK);
				
				if (i > MAX_ELEMENTS_IMPORTED) {
					break;
				} else {
					Matcher m = r.matcher(line);
					if (!m.find()) {
						System.out.println("N�o encontrado o padr�o na linha: " + line);
						continue;
					}
					for (int j = 1; j <= 2; j++) {
						String w = m.group(j);
						int lastIndexOfSpare = w.lastIndexOf(str1);

						prefix = w.substring(0, lastIndexOfSpare);
						posfix = w.substring(lastIndexOfSpare, w.length());
						if (prefixesMap.get(prefix) == null) {
							prefixCounter += 1;
							prefixesMap.put(prefix, prefixCounter);
						}
						sb.setLength(0);
						linkCompact = sb.append(prefixesMap.get(prefix)).append(DOUBLE_DOT_SEPARATOR).append(posfix)
								.toString();
						idLink = linkIdMap.get(linkCompact);
						if (idLink == null) {
							linkCounter++;
							linkIdMap.put(linkCompact, linkCounter);
							idLink = linkCounter;
						}

						if (j == 1) {
							idFromLink = idLink;
						} else if (j == 2) {
							idToLink = idLink;
						}
					}
					sb.setLength(0);
					writer.println(sb.append(idFromLink).append(LINK_SEPARATOR).append(idToLink).toString());
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.close();
			}
			Date dateEnd = Calendar.getInstance().getTime();
			System.out.println(dateStart);
			System.out.println(dateEnd);

			System.out.println("Total de prefixos:" + prefixesMap.size());
			System.out.println("Total de links:" + linkCounter);
			System.out.println("Link Id Map size:" + linkIdMap.size());
		}
		try {
			clearDatabase();
			importPrefixes(prefixesMap);
			prefixesMap = null;			
			importResourceLinks(linkIdMap);
			linkIdMap =  null;
			importWikiLinks();
		} catch (SQLException e) {
			System.out.println("Falha no SQL");
			e.printStackTrace();
		}
		return i;
	}

	private void importPrefixes(Map<String, Integer> prefixesMap) throws SQLException {
		ConnectorSQL connector = new ConnectorSQL();
		Connection conn = connector.getConnection();
		conn.setUseOldUTF8Behavior(true);

		String str0 = "INSERT INTO prefix (id, descricao) VALUES ";
		String str1 = COMMA_SEPARATOR;
		String str2 = SQL_PART__TWO_PARAMETERS;
		String[] prefixes = new String[SQL_BLOCK_SIZE];
		PreparedStatement ps = null;					
		try {
			StringBuilder sb = new StringBuilder();					
			sb.append(str0);
			for (int i = 0; i < SQL_BLOCK_SIZE; i++) {
				if (i > 0)
					sb.append(str1);
				sb.append(str2);
			}					
			ps = conn.prepareStatement(sb.toString());
			int j = 0;
			for (Entry<String, Integer> pref : prefixesMap.entrySet()) {
				prefixes[j] = pref.getKey();
				j++;
				if (j % SQL_BLOCK_SIZE == 0) {
					for (int k = 1; k <= j; k++) {
						ps.setInt((k * 2) - 1, prefixesMap.get(prefixes[k - 1]));
						ps.setString((k * 2), prefixes[k - 1]);
					}
					ps.execute();
					j = 0;
					sb = null;					
				}

			}
			if (j != 0) {
				sb = null;
				sb = new StringBuilder();					
				sb.append(str0);
				for (int i = 0; i < j; i++) {
					if (i > 0)
						sb.append(str1);
					sb.append(str2);
				}
				if (ps != null) {
					ps.close();
					ps = null;
				}				
				ps = conn.prepareStatement(sb.toString());
				for (int k = 1; k <= j; k++) {
					ps.setInt((k * 2) - 1, prefixesMap.get(prefixes[k - 1]));
					ps.setString((k * 2), prefixes[k - 1]);
				}
				ps.execute();				
			}
			System.out.println("Terminou a importa�a� de prefixos com sucesso.");
		} catch (SQLException e) {
			System.out.println("Exce��o ocorreu ao executar essa query.");
			e.printStackTrace();
		} finally {
			ps.close();
			ps = null;
			connector.close();
			connector = null;
			prefixes = null;
		}
	}

	public void importResourceLinks(Map<String, Integer> linkIdMap) throws SQLException {
		ConnectorSQL connector = new ConnectorSQL();
		Connection conn = connector.getConnection();
		conn.setUseOldUTF8Behavior(true);
		String str0 = "INSERT INTO resourcelink (id, uri) VALUES ";
		String str1 = COMMA_SEPARATOR;
		String str2 = SQL_PART__TWO_PARAMETERS;
		int j = 0;
		String[] links = new String[SQL_BLOCK_SIZE];
		PreparedStatement ps = null;
		StringBuilder sb = new StringBuilder();
		
		sb.append(str0);
		for (int i = 0; i < SQL_BLOCK_SIZE; i++) {
			if (i > 0)
				sb.append(str1);
			sb.append(str2);
		}			
		int q = 0;
		ps = conn.prepareStatement(sb.toString());		
		for (Entry<String, Integer> lk : linkIdMap.entrySet()) {
			links[j] = lk.getKey();
			j++;
			q++;
			if (j % SQL_BLOCK_SIZE == 0) {
				ps.clearParameters();
				for (int k = 1; k <= j; k++) {
					ps.setInt((k * 2) - 1, linkIdMap.get(links[k - 1]));
					ps.setString((k * 2), links[k - 1]);
				}
				ps.execute();
				j = 0;
				sb = null;				
			}

		}
		if (j != 0) {
			sb = new StringBuilder();					
			sb.append(str0);
			for (int i = 0; i < j; i++) {
				if (i > 0)
					sb.append(str1);
				sb.append(str2);
			}	
			if (ps != null) {
				ps.close();
			}
			ps = conn.prepareStatement(sb.toString());			
			ps.clearParameters();
			for (int k = 1; k <= j; k++) {
				ps.setInt((k * 2) - 1, linkIdMap.get(links[k - 1]));
				ps.setString((k * 2), links[k - 1]);
			}
			ps.execute();
			j = 0;
			sb = null;				
		}
		
		ps.close();		
		connector.close();
		conn = null;
		ps = null;
		connector = null;
		links = null;
		System.out.println("Terminou a importa��o de resource links com sucesso.");		
	}

	public void importWikiLinks() throws SQLException {
		ConnectorSQL connector = new ConnectorSQL();
		Connection conn = connector.getConnection();

		String str0 = "INSERT INTO wikilink (resourcelinkfrom_id, resourcelinkto_id) VALUES ";
		String str1 = COMMA_SEPARATOR;
		String str2 = SQL_PART__TWO_PARAMETERS;

		int k = 0;
		String[] resourceLinks = null;
		conn.setUseOldUTF8Behavior(true);		
		int qtd = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_PATH_LINKS_FOUND))) {
			String line;
			StringBuilder sb = new StringBuilder();
			
			resourceLinks = new String[SQL_BLOCK_SIZE];			
			sb.append(str0);
			for (int i = 0; i < SQL_BLOCK_SIZE; i++) {
				if (i > 0)
					sb.append(str1);
				sb.append(str2);
			}
			String fullBlock = sb.toString();
			PreparedStatement ps = conn.prepareStatement(fullBlock);			
			while ((line = br.readLine()) != null) {
				qtd++;
				resourceLinks[k++] = line;
				if (k == SQL_BLOCK_SIZE) {			
					ps.clearParameters();
					for (int j = 1; j <= k; j++) {
						String[] rLArray = resourceLinks[j - 1].split(REGEX_NOT_NUMBER);
						ps.setInt((j * 2) - 1, Integer.valueOf(rLArray[0]));
						ps.setInt((j * 2), Integer.valueOf(rLArray[1]));
					}
					ps.execute();					
					k = 0;
				}
			}				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (k != 0) {
			StringBuilder sb = new StringBuilder();			
			sb.append(str0);
			for (int i = 0; i < k; i++) {
				if (i > 0)
					sb.append(str1);
				sb.append(str2);
			}
			PreparedStatement ps = conn.prepareStatement(sb.toString());

			for (int j = 1; j <= k; j++) {
				String[] rLArray = resourceLinks[j - 1].split(REGEX_NOT_NUMBER);
				ps.setInt((j * 2) - 1, Integer.valueOf(rLArray[0]));
				ps.setInt((j * 2), Integer.valueOf(rLArray[1]));
			}
			ps.execute();
			ps = null;		
			sb = null;
		}
		connector.close();
		System.out.println("Terminou a importa��o de wiki links com sucesso.");		
	}	

	public static void main(String[] args) {
		String fileName = "C:\\Users\\Tiago\\Downloads\\Documents\\Mestrado\\Dados\\page_links_en.ttl";
		Importator imp = new Importator(fileName);

		imp.findWikiLinkResources("http://dbpedia.org/resource/Poverty");
		//Long qtdLines = imp.importForDB();
		//System.out.println("Total de linhas:" + qtdLines);
		System.out.println("The end!");
	}	
}
